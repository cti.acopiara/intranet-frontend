import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// local modules
import auth from './modules/auth'
import agenda from './modules/agenda'
import branches from './modules/branches'
import frequencies from './modules/frequencies'
import informatives from './modules/informatives'
import tickets from './modules/tickets'
import settings from './modules/settings'

export default new Vuex.Store({
  modules: {
    agenda,
    branches,
    frequencies,
    informatives,
    tickets,
    settings,
    auth
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
