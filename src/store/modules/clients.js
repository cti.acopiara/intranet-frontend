// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  clients: [],
  client: Object,
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getClients ({commit}) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/clients/'
    /*    var options = {
      withCredentials: true
    }
    */
    axios.get(url)
      .then(response => {
        commit('setClients', response.data)
        return response
      })
      .catch(error => {
        commit('setError', true)
        // commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - index client controller'
        messages.showSnackbar(message, 'warning')
        return error 
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  getClient ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/clients/' + payload.toString() + '/'
    axios.get(url)
      .then(response => {
        console.log(response.data)
        commit('setClient', response.data)
      })
      .catch(error => {
        console.log('error:', error.toString())
        commit('setError', true)
        commit('setErrorMessage', error.toString().split(':')[1])
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  createClient ({commit}, payload) {
    commit('setLoading', true)
    axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    var url = Vue.config.urlBase + '/api/clients/create/'
    var params = {
      'cnpj': payload.cnpj,
      'name': payload.name,
      'contact_name': payload.contact_name,
      'contact_email': payload.contact_email,
      'contact_phone': payload.contact_phone,
    }
    var data = Object.entries(params)
      .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
      .join('&');

    axios.post(url, data)
    .then(function (response) {
      if (response.status === 200) {
        var message = 'Cliente criado com sucesso.'
        messages.showSnackbar(message, 'success')
      }
      router.push({ name: 'index-clients' })
      return response
    })
    .catch(function (error) {
      if (error.response.status === 403) {
        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - create controller'
        messages.showSnackbar(message, 'warning')
      } 
    })
    .finally(() => {
      commit('setLoading', false)
    })
  },
  updateClient ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/clients/update/' + payload.id + '/'
      var params = {
      'cnpj': payload.cnpj,
      'name': payload.name,
      'contact_name': payload.contact_name,
      'contact_email': payload.contact_email,
      'contact_phone': payload.contact_phone,
    }
    var data = Object.entries(params)
      .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
      .join('&');

    axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    axios.post(url, data)
    .then(function (response) {
      if (response.status === 200) {
        var message = 'Cliente atualizado com sucesso.'
        messages.showSnackbar(message, 'success')
      }
      router.push({ name: 'index-clients' })
      return response
    })
    .catch(function (error) {
      if (error.response.status === 403) {
        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - update controller'
        messages.showSnackbar(message, 'warning')
      } 
    })
    .finally(() => {
      commit('setLoading', false)
    })
  },
  deleteClient ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/clients/delete/' + payload.id + '/'

    axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    axios.post(url)
    .then(function (response) {
      if (response.status === 200) {
        var message = 'Cliente deletado com sucesso.'
        messages.showSnackbar(message, 'success')
      }
      router.push({ name: 'index-clients' })
      return response
    })
    .catch(function (error) {
      if (error.response.status === 403) {
        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - create controller'
        messages.showSnackbar(message, 'warning')
      } 
    })
    .finally(() => {
      commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setClients (state, payload) {
    state.clients = payload
  },
  setClient (state, payload) {
    state.client = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
