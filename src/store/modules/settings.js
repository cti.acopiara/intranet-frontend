// import things here
import Vue from 'vue'
import axios from 'axios'

import messages from '@/utils/messages'

const state = {
  settings: [],
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getSettings ({commit}) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/configuracoes/'
    var options = {
      headers: {
        Accept: 'application/json',
      }
    }
    axios.get(url, options)
      .then(response => {
        if (Object.keys(response.data).length === 0 && response.data.constructor === Object) {
          commit('setSettings', false)
        } else {
          commit('setSettings', response.data)
        }
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
}

// mutations
const mutations = {
  setSettings (state, payload) {
    state.settings = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
