// import things here
import Vue from 'vue'
import axios from 'axios'

import messages from '@/utils/messages'

const state = {
  informatives: [],
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  printReport ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/frequencias/'
    var options = {
      headers: { 
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
      },
			responseType: 'blob',
    } 

		const params = new URLSearchParams()
		params.append('name', payload.name.toUpperCase())
		params.append('username', payload.username)
		params.append('responsability', payload.responsability)
		params.append('sector', payload.sector)
		params.append('role', payload.role)
		params.append('month', payload.month)
		params.append('year', payload.year)
		params.append('work_schedule', payload.work_schedule)
		params.append('work_load', payload.work_load)
		params.append('work_shift', payload.work_shift)
		params.append('vacation', payload.vacation)
		if (payload.vacation == 's') {
			params.append('start_vacation', payload['start_vacation'])
			params.append('end_vacation', payload['end_vacation'])
		}

    axios.post(url, params, options)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url;
        var pdf_filename = payload.username + '_' + payload.month.toString() + payload.year.toString() + '.pdf'
        link.setAttribute('download', pdf_filename)
        document.body.appendChild(link)
        link.click()
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - printReport'
        messages.showSnackbar(message, 'is-danger')
        return error 
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
}

// mutations
const mutations = {
  setInformatives (state, payload) {
    state.informatives = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
