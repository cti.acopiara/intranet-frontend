import jsCaptcha from 'js-captcha'

var myCaptcha = new jsCaptcha({
  el: '.jCaptcha',
  canvas: {
    class: 'jCaptchaCanvas',
    style: {
      // required properties for captcha stylings:
      width: 100,
      height: 15,
      textBaseline: 'top',
      font: '15px Arial',
      textAlign: 'left',
      fillStyle: '#ddd'
    }
  },
  // set callback function for success and error messages:
  callback: ( response, $captchaInputElement, numberOfTries ) => {
    if ( response == 'success' ) {
      // success handle, e.g. continue with form submit
    }
    if ( response == 'error' ) {
      // error handle, e.g. add error class to captcha input

      if (numberOfTries === 3) {
        // maximum attempts handle, e.g. disable form
      }
    }
  }
})

export default {
  showCapcha: function () {
    console.log(myCaptcha)
  }
}
