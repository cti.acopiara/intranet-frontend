import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import './registerServiceWorker'

// External Libs
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
import VAnimateCss from 'v-animate-css'
Vue.use(VAnimateCss)

// Bulma and Buefy - Theme
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy, {
	defaultMonthNames: [
		'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
	],
	defaultDayNames: [
		'Dom',
    'Seg',
    'Ter',
    'Qua',
    'Qui',
    'Sex',
    'Sáb',
	]
})

// Local styles and colors
require('./assets/sass/main.scss')

// Global components
import Layout from '@/layouts/Layout'
import TheFooter from '@/components/TheFooter.vue'
import TheNavbar from '@/components/TheNavbar.vue'
import TheSidebar from '@/components/TheSidebar.vue'
import TheHeader from '@/components/TheHeader.vue'
import TheTable from '@/components/TheTable.vue'
import TheCard from '@/components/TheCard.vue'
import TheBox from '@/components/TheBox.vue'
import TheBackButton from '@/components/TheBackButton'
import TheLogo from '@/components/TheLogo.vue'
import TheUnderConstruction from '@/components/TheUnderConstruction.vue'
import IFLinks from '@/components/IFLinks.vue'
import IFNotifications from '@/components/IFNotifications.vue'
import IFSettings from '@/components/IFSettings.vue'

import TheAdminTable from '@/components/Admin/TheTable.vue'
import TheAdminNavbar from '@/components/Admin/TheNavbar.vue'

// Registering global components
Vue.component('layout', Layout)
Vue.component('the-navbar', TheNavbar)
Vue.component('the-footer', TheFooter)
Vue.component('the-sidebar', TheSidebar)
Vue.component('the-header', TheHeader)
Vue.component('the-table', TheTable)
Vue.component('the-card', TheCard)
Vue.component('the-box', TheBox)
Vue.component('the-back-button', TheBackButton)
Vue.component('the-logo', TheLogo)
Vue.component('the-under-construction', TheUnderConstruction)
Vue.component('if-links', IFLinks)
Vue.component('if-notifications', IFNotifications)
Vue.component('if-settings', IFSettings)

// Registering global admin components
Vue.component('the-admin-table', TheAdminTable)
Vue.component('the-admin-navbar', TheAdminNavbar)

Vue.config.productionTip = false
Vue.config.urlBase = process.env.VUE_APP_BACKEND_URL

// Check local storage to handle refreshes
if(window.localStorage) {
  var localUserString = window.localStorage.getItem('user') || 'null'
  var localUser = JSON.parse(localUserString)

  if (localUser && store.state.auth.user !== localUser) {
    store.commit('setUser', localUser)
    store.commit('setIsAuthenticated', window.localStorage.getItem('isAuthenticated'))
  }
}

// Check if user is authenticated
/*
router.beforeEach(function(to, from, next) {
  console.log('isAuthenticated: ' + store.getters.isAuthenticated)
  //console.log('to.path ' + to.path)
  // if (store.getters.isAuthenticated) {
  if (to.path !== '/products') {
    next({name: 'index-products'});
  } else {
    next();
  }
});
*/

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
