# intranet-frontend

Esse projeto foi feito utilizando a linguagem Javascript e framework VueJS.
Você pode contribuir, basta ter conhecimentos de Javascript, se já conhecer o
framework, melhor. Faça um *fork* desse projeto e siga os passos abaixo.

```
yarn install
yarn run serve
```

Com isso você já pode visualizar no navegador a aplicação rodando. Faça as 
devidas modificações e mande um *pull-request* para o repositório inicial.

